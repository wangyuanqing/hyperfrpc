<?php

namespace RpcInterface;

/**
 * 系统附件服务接口
 * Class AttachmentRpcServiceInterface
 * @Author wyqing
 * @package RpcInterface
 */
interface AttachmentRpcServiceInterface
{
    public function getAttachmentPath(array $parame , array $headers): array ;
    public function checkAttachment(array $parame , array $headers): array ;
    public function uploadAttachment(array $parame , array $headers): array ;
}
