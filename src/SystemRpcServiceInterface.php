<?php

namespace RpcInterface;

/**
 * 系统服务接口
 * @Author wyqing
 * Class SystemRpcServiceInterface
 * @package Hyperfrpc
 */
interface SystemRpcServiceInterface
{
    public function getOneConfigValue(array $parame , array $headers):array ;
    public function getMultipleConfigValue(array $parame , array $headers): array ;
    public function getAllConfigValue(array $parame , array $headers): array ;
    public function getConfigList(array $parame , array $headers): array ;
    public function getConfigDetail(array $parame , array $headers): array ;
    public function deleteConfig(array $parame , array $headers): array ;
}
