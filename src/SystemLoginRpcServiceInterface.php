<?php


namespace RpcInterface;

/**
 * 系统用户登录服务接口
 * Interface SystemLoginRpcServiceInterface
 * @Author wyqing
 * @package RpcInterface
 */
interface SystemLoginRpcServiceInterface
{
    public function systemLogin(array $parame , array $headers): array ;
    public function systemAutoLogin(array $parame , array $headers): array ;
    public function systemOutLogin(array $parame , array $headers): array ;
}
