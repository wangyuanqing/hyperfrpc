<?php

namespace RpcInterface;

/**
 * 系统服务接口
 * Class ConfigRpcServiceInterface
 * @Author wyqing
 * @package Hyperfrpc
 */
interface ConfigRpcServiceInterface
{
    public function getConfigs(array $parame , array $headers):array ;
    public function getMultipleConfigs(array $parame , array $headers):array ;
    public function getConfigsLists(array $parame , array $headers):array ;
    public function getConfigsItem(array $parame , array $headers):array ;
    public function saveConfigs(array $parame , array $headers):array ;
}
