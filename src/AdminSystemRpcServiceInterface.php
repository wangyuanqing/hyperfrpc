<?php


namespace RpcInterface;

/**
 * Admin系统服务相关接口服务
 * Interface AdminSystemRpcServiceInterface
 * @package RpcInterface
 */
interface AdminSystemRpcServiceInterface
{
    public function getSystemJumpUrl(array $parame , array $headers): array ;

    public function getAuthMenus(array $parame , array $headers): array ;

    public function getSidebarMenu(array $parame , array $headers): array ;

    public function getUserInfo(array $parame , array $headers): array ;

    public function getMessageCount(array $parame , array $headers): array ;
}
